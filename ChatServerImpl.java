/*
 * 	@author	: Zealous
 *
 * 	Date	: August 2,2013
 *
 * 	Desc.	: Implementation of ChatServer interface
 *	
 */
public class ChatServerImpl extends UnicastRemoteObject implements 
	ChatServer{

	public void chatServerHello(){
		System.out.println("Hello from client");
	}
}

