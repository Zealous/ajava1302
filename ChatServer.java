/**
 * 	author	: Zealous Devotee
 *
 * 	Date	: August 2,2013
 *
 * 	Desc.	: ChatServerClient.java interface
 * 	
 */

public interface ChatServer extends Remote throws RemoteException{
	
	public void chatServerHello();

}


